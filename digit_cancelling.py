import time
start_time = time.time()

def simplification(numerator,denominator):
    for i in list(str(numerator)):
        if i in list(str(denominator)):
            if i == '0':
                return 0
            else:
                list_num = list(str(numerator))
                list_den = list(str(denominator))
                list_den.remove(i)
                list_num.remove(i)
                new_den = float(list_den[0])
                new_num = float(list_num[0])
                if new_den == 0:
                    return 0
                else:
                    return float(new_num/new_den)

def list_digit_cancelling():
    list_digit = []
    for i in range(11,100):
        for j in range(10,i):
            if (j / i) == simplification(j,i):
                list_digit.append([j,i])
    return list_digit

print(list_digit_cancelling())
print(f"--- {(time.time() - start_time):.10f} seconds ---" )